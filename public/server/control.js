function clickTank() {
    // Solution for Handling Events.
    var sceneEl = document.querySelector('a-scene'); 
    var tanqueV = sceneEl.querySelector('#tank');
    
    
    tanqueV.setAttribute('visible', !tanqueV.getAttribute('visible'));
  }

function clickPump() {
    // Solution for Handling Events.
    var sceneEl = document.querySelector('a-scene'); 
    var pumpV = sceneEl.querySelector('#motor');
    
    
    pumpV.setAttribute('visible', !pumpV.getAttribute('visible'));
}

function clickHydran() {
  // Solution for Handling Events.
  var sceneEl = document.querySelector('a-scene'); 
  var hydranV = sceneEl.querySelector('#hydran');
  
  
  hydranV.setAttribute('visible', !hydranV.getAttribute('visible'));
}

document.querySelector('#tank').addEventListener('click', function () {
    var sceneEl = document.querySelector('a-scene'); 
    var tanquePanel = sceneEl.querySelector('#tankP');
    if(this.getAttribute('visible')){
      tanquePanel.setAttribute('visible',!tanquePanel.getAttribute('visible'));
    }
  });

document.querySelector('#tankP').addEventListener('click', function () {
    this.setAttribute('visible',!this.getAttribute('visible'))
  });

document.querySelector('#motor').addEventListener('click', function () {
    var sceneEl = document.querySelector('a-scene'); 
    var motorPanel = sceneEl.querySelector('#motorP');
    if(this.getAttribute('visible')){
      motorPanel.setAttribute('visible',!motorPanel.getAttribute('visible'));
    }
  });

document.querySelector('#motorP').addEventListener('click', function () {
    this.setAttribute('visible',!this.getAttribute('visible'))
  });

  document.querySelector('#hydran').addEventListener('click', function () {
    var sceneEl = document.querySelector('a-scene'); 
    var hydranPanel = sceneEl.querySelector('#hydranP');
    if(this.getAttribute('visible')){
      hydranPanel.setAttribute('visible',!hydranPanel.getAttribute('visible'));
    }
  });

document.querySelector('#hydranP').addEventListener('click', function () {
    this.setAttribute('visible',!this.getAttribute('visible'))
  });

  document.querySelector('#hydran').addEventListener('model-loaded', function() {
    //document.getElementById("loader").style.display = "none";
    document.getElementById("blocker").className = "animate-top";
    setTimeout(function(){ document.getElementById("blocker").style.display = "none"; }, 1000);
  });